-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 09-Mar-2019 às 01:16
-- Versão do servidor: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ofertas`
--

CREATE TABLE `ofertas` (
  `id` int(10) NOT NULL,
  `img_card` varchar(10) NOT NULL,
  `titulo_card` varchar(50) NOT NULL,
  `msg_card` varchar(500) NOT NULL,
  `preco_card` float NOT NULL,
  `ultima_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `ofertas`
--

INSERT INTO `ofertas` (`id`, `img_card`, `titulo_card`, `msg_card`, `preco_card`, `ultima_mod`) VALUES
(1, 'img11.jpg', 'Para se Aventurar', 'O vento batendo no rosto, a sensação de liberdade. Pacote ideial para você que se desafia em meio natureza. Com essa escolha sua viagem será uma aventura memorável, você ira conhecer lugares onde os limites humanos serão desafiados, onde a cada dia você vai sentir que quebrou os proprios limites para melhor. Começamos com um investimento minimo de', 3500, '2019-03-05 13:12:26'),
(2, 'img12.jpg', 'Para Curtir', 'Aproveite todo o clima tropical, desde prais a noites badaladas de festas tradicionais. Com essa escolha sua viagem será dedicada a curtir os lugares mais badalados da cidade que estará, esse pacote é focado em otimizar o máximo de atrações e locais para você curtir só ou acompanhado. Começamos com um investimento minimo de', 4500, '2019-03-05 13:12:31'),
(3, 'img13.jpg', 'Para Descobrir', 'Pise onde poucos pisaram, descubra paisagens que poucos descobriram. Com essa escolha sua viagem será uma descoberta de si mesmo e do meio a sua volta, locais onde poucos passaram, paisagens que tiram o folego e nos faz refletir em como o mundo pe grande. Começamos com um investimento minimo de', 4000, '2019-03-05 13:12:37'),
(4, 'img14.jpg', 'Para Relaxar', 'Relaxe em praias deslubrantes, um lugar perfeito para recarregar as energias. Com essa escolha sua viagem será relaxante, um momento de lazer e descanso, lugares que pensam na melhor forma de acolher aqueles que querem apenas descansar e repor as energias. Começamos com um investimento minimo de', 6000, '2019-03-05 13:12:42'),
(5, 'img15.jpg', 'Para Conhecer', 'Conheça culturas e antigos costumes, ideal para você que gosta de conhecer o passado. Com essa escolha sua viagem será cheia de experiencias e conhecimentos sobre novas culturas e seus costumes. Conhecerá o passado e os povos que originaram a nossa civilização. Começamos com um investimento minimo de', 5500, '2019-03-05 13:12:47'),
(6, 'img16.jpg', 'Para Comemorar', 'Venha comemorar a chegada de um novo ano, lugar ideal para se iniciar um novo ciclo. Com essa escolha sua viagem será uma festa, comemorando o novo ciclo que está por vir e as consquistas do ciclo que passou, muita alegria e celebração em um novo ano. Comemore o Ano Novo em um novo lugar. Começamos com um investimento minimo de', 9000, '2019-03-05 13:12:59'),
(7, 'img9.jpg', 'Oferta para Testes de Alteração', 'Esse card é para testar a possibilidade de altera-lo', 7850, '2019-03-09 00:12:59');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pagina_contato`
--

CREATE TABLE `pagina_contato` (
  `id` int(10) NOT NULL,
  `titulo_contato` varchar(50) NOT NULL,
  `msg_contato` varchar(200) NOT NULL,
  `ultima_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pagina_contato`
--

INSERT INTO `pagina_contato` (`id`, `titulo_contato`, `msg_contato`, `ultima_mod`) VALUES
(1, 'Entre em contato', 'Você tem alguma dúvida? Entre com contato conosco para estar mais próximo de realizar o sonho de viajar.', '2019-03-04 21:21:59');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pagina_principal`
--

CREATE TABLE `pagina_principal` (
  `id` int(10) NOT NULL,
  `rod_esq_titulo` varchar(50) NOT NULL,
  `rod_esq_msg` varchar(200) NOT NULL,
  `rod_dir_titulo` varchar(50) NOT NULL,
  `rod_dir_msg` varchar(200) NOT NULL,
  `video` varchar(10) NOT NULL,
  `ultima_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pagina_principal`
--

INSERT INTO `pagina_principal` (`id`, `rod_esq_titulo`, `rod_esq_msg`, `rod_dir_titulo`, `rod_dir_msg`, `video`, `ultima_mod`) VALUES
(1, 'VIAJAR É VIVER', '“O uso das viagens serve para regular a imaginação através da realidade, e assim ao invés de imaginar como as coisas talvez sejam, você vê como elas realmente são.”', 'VIVER É VIAJAR', '“Entre todos os livros do mundo, as melhores histórias estão entre as páginas de um passaporte.”', '1', '2019-03-04 21:28:24');

-- --------------------------------------------------------

--
-- Estrutura da tabela `quem_somos`
--

CREATE TABLE `quem_somos` (
  `id` varchar(10) NOT NULL,
  `imagem_quemsomos` varchar(20) NOT NULL,
  `quemsomos_msg` varchar(2000) NOT NULL,
  `ultima_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `quem_somos`
--

INSERT INTO `quem_somos` (`id`, `imagem_quemsomos`, `quemsomos_msg`, `ultima_mod`) VALUES
('1', '17', 'Somos uma equipe preparada para disponibilizar aos nossos clientes novas experiências. Uma nova forma de viajar. Você precisa apenas escolher qual tipo de viagem gostaria, com aventura, para relaxar, para conhecer novas culturas, para cutir, o que desejar.Então, com base nessa escolha e no valores investidos que serão disposnibilizados por nossos clientes, planejamos o local da viagem, onde ficarão e etc, sem dores de cabeça. Dessa forma proporcionamos uma surpresa com o melhor custo beneficio disponivel com os valores disponibilizados. Queremos proporcionar a nossos clientes uma grande e agradável surpresa de viajar com tranquilidade, sem preocupações e para o melhor destino possivel. Explore nossas alternativas, entre em contato e tenha uma ótima experiência assim como muitos já tiveram. Estamos ansiosos para realizar o seu sonho.', '2019-03-04 21:31:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ofertas`
--
ALTER TABLE `ofertas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pagina_contato`
--
ALTER TABLE `pagina_contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pagina_principal`
--
ALTER TABLE `pagina_principal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quem_somos`
--
ALTER TABLE `quem_somos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ofertas`
--
ALTER TABLE `ofertas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
