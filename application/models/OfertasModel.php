<?php 
    defined('BASEPATH') OR exit('No direct script acasse allowed');

    class OfertasModel extends CI_Model{

        var $id;
        var $img_card;
        var $titulo_card;
        var $msg_card;
        var $preco_card;

        public function get_data(){
            $sql = "SELECT * FROM ofertas" ;
            $res = $this->db->query($sql);
            $data = $res->result();
            return $data;

        }

        
        public function nova_oferta(){
            if(sizeof($_POST)==0) return;

            $data['img_card'] = $this->input->post("img_card");
            $data['titulo_card'] = $this->input->post("titulo_card");
            $data['msg_card'] = $this->input->post("msg_card");
            $data['preco_card'] = $this->input->post("preco_card");
            $this->db->insert('ofertas',$data);


        }

        public function excluir_oferta($id){
            $this->db->where("id",$id);
            $this->db->delete("ofertas");

        }

        

        public function get_detalhe($id){
            $sql = "SELECT * FROM ofertas WHERE id = $id";
            $res = $this->db->query($sql);
            $data = $res->result();
            return $data;

        }
    

    public function alterar_oferta($id)
    {
        $data = $this->get_detalhe($id);       
        
        $newdata = $this->input->post();

        if($data == $newdata) return false;
        if($newdata == null) return false;

        else
        {   $this->db->set('id', $id);
            $this->db->set('img_card', $this->input->post('img_card'));
            $this->db->set('titulo_card', $this->input->post('titulo_card'));
            $this->db->set('msg_card', $this->input->post('msg_card'));
            $this->db->set('preco_card', $this->input->post('preco_card'));
            $this->db->where('id', $id);
            $this->db->update('ofertas');

            return true;
        }        
    }
}