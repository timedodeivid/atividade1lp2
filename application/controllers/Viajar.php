<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Viajar extends CI_Controller
{

    public function index()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        /*$this->load->model('ViajarModel');
        $data = $this->ViajarModel->get_data($id);
        $v['pagina_principal'] = $this->load->view('viajar/introd', $data , true);      
        $v['quem_somos'] = $this->load->view('viajar/empresaviagens', $data, true);   
        $v['ofertas'] = $this->load->view('produto/ofertasviagens', $data, true);
        $v['pagina_contato'] = $this->load->view('produto/ofertasviagens', $data, true);*/

        $this->load->view('viajar/introd');
        $this->load->view('common/footer');
        $this->load->view('common/rodape');
    }
    
    public function quemsomos()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');

       /* $this->load->model('ViajarModel');
        $data = $this->ViajarModel->get_data($id);
        $v['pagina_principal'] = $this->load->view('viajar/introd', $data , true);      
        $v['quem_somos'] = $this->load->view('viajar/empresaviagens', $data, true);   
        $v['ofertas'] = $this->load->view('produto/ofertasviagens', $data, true);
        $v['pagina_contato'] = $this->load->view('produto/ofertasviagens', $data, true);*/

        $this->load->view('viajar/empresaviagens');
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    public function compra()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('OfertasModel');
        $data['ofertas'] = $this->OfertasModel->get_data();                      
                
       // $v['tabela'] = $this->load->view('viajar/ofertasviagens', $data, true);
        
        //$v['titulo'] = "Lista de Ofertas";
                
        $this->load->view('viajar/ofertasviagens',$data);      
                
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    public function contato()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        /*$this->load->model('ViajarModel');
        $data = $this->ViajarModel->get_data($id);
        $v['pagina_principal'] = $this->load->view('viajar/introd', $data , true);      
        $v['quem_somos'] = $this->load->view('viajar/empresaviagens', $data, true);   
        $v['ofertas'] = $this->load->view('produto/ofertasviagens', $data, true);
        $v['pagina_contato'] = $this->load->view('produto/ofertasviagens', $data, true);*/

        $this->load->view('viajar/contatoviagens');
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    public function admincadastrar()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('OfertasModel');
        //realiza a atualização do bd
        $this->OfertasModel->nova_oferta();
               
        $this->load->view('viajar/administradorcadastrar');
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    public function adminalterar($id)
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('OfertasModel');

                       
         
        $this->load->view('viajar/administradoralterar',$id);

        $this->OfertasModel->alterar_oferta($id);
			
        
                       
        $this->load->view('common/rodape');
        $this->load->view('common/footer');

        
    }

    

    public function adminexcluir($id)
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('OfertasModel');

        $this->OfertasModel->excluir_oferta($id);

        $this->load->view('viajar/administradorexcluir');
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    public function adminlistar()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('OfertasModel');

        $data['ofertas'] = $this->OfertasModel->get_data();                     
                      
        $this->load->view('viajar/administradorlistar',$data); 
        
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }




    

        public function cria_setor(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->model('Viajarmodel');
            $this->Estoquemodel->novo_setor();
            $this->load->view('viajar/administradorviagens');
            $this->load->view('common/rodape');
            $this->load->view('common/footer');

        }
        public function alterar_tabela(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->model('ViajarModel');
            //realiza a atualização do bd
            $this->ViajarModel->alterar_tabela();
            //carrega os dados do registro a ser editado
            $data = $this->ViajarModel->get_data($id);
            $this->ViajarModel->alterar_tabela();
            $this->load->view('viajar/administradorviagens',$data);
            $this->load->view('common/rodape');
            $this->load->view('common/footer');


            function alterar() {
                         
                $data['id'] = $this->input->post('id');
                $data['img_card'] = $this->input->post('img_card');
                $data['titulo_card'] = $this->input->post('titulo_card');
                $data['msg_card'] = $this->input->post('msg_card');
                $data['preco_card'] = $this->input->post('preco_card');
         
                $this->load->model('OfertasModel');
                if ($this->OfertasModel->alterar($data)) {
                    redirect('Viajar');
                } else {
                    log_message('error', 'Erro na alteração...');
                }
            }
        }

        
}


