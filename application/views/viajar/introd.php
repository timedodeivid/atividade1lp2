
<div id="video-carousel-example" class="carousel slide carousel-fade" data-ride="carousel">
  
  <ol class="carousel-indicators">
    <li data-target="#video-carousel-example" data-slide-to="0" class="active"></li>
    <li data-target="#video-carousel-example" data-slide-to="1"></li>
    <li data-target="#video-carousel-example" data-slide-to="2"></li>
  </ol>
  

  
  
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <video class="video-fluid" autoplay loop muted>
        <source src="<?= base_url('assets/video/video1.mp4')?>" type="video/mp4" />
      </video>
      <div class="carousel-caption">
        <div class="animated fadeInDown">
          <h3 class="h3-responsive">Lugares para se Deslumbrar</h3>
          
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <video class="video-fluid" autoplay loop muted>
        <source src="<?= base_url('assets/video/video4.mp4')?>" type="video/mp4" />
      </video>
      <div class="carousel-caption">
        <div class="animated fadeInDown">
          <h3 class="h3-responsive">Lugares para se Aventurar</h3>
          
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <video class="video-fluid" autoplay loop muted>
        <source src="<?= base_url('assets/video/video3.mp4')?>" type="video/mp4" />
      </video>
      <div class="carousel-caption">
        <div class="animated fadeInDown">
          <h3 class="h3-responsive">Lugares para Relaxar</h3>
          
        </div>
      </div>
    </div>
  </div>
  
  <a class="carousel-control-prev" href="#video-carousel-example" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#video-carousel-example" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  
</div>
