
<div class="row">



    <?php foreach ($ofertas as $oferta): ?>
    <div class="col-12 col-md-4">


        <div class="card mb-4">

        
            <div class="view overlay">
                <img class="card-img-top img-fluid" src="<?= base_url('assets/img/'.$oferta->img_card)?>" alt="Card image cap">
                
            </div>

            
            <div class="card-body">

                
                <h4 class="card-title"><?= $oferta->titulo_card ?></h4>
                
                <p class="card-text"><?= $oferta->msg_card ?> R$ <?= $oferta->preco_card ?>
                </p>
                
                <a class="button-link" href="<?= base_url('Viajar/contato')?>"><button type="button" class="btn btn-default px-3">Fazer orçamento!</button>
                </a>

            
            </div>

        </div>

            

    </div>

        <?php  endforeach;  ?>
</div>  



                        
    <!-- Código que possibilita modificar praticamente todo detalhe da página poderá ser emplementado no futuro-->
    
    <!--<div class="card mb-4">

        
        <div class="view overlay">
            <img class="card-img-top" src="<?= base_url('assets/img/logo12.jpg')?>" alt="Card image cap">
            
        </div>

        
        <div class="card-body">

            
            <h4 class="card-title">Para Curtir</h4>
            
            <p class="card-text">Aproveite todo o clima tropical, desde prais a noites badaladas de festas tradicionais.
                    Com essa escolha sua viagem será dedicada a curtir os lugares mais badalados da cidade que estará, esse pacote é focado
                    em otimizar o máximo de atrações e locais para você curtir só ou acompanhado.

                    Começamos com um investimento minimo de R$ 4500,00
            </p>
            
            <a class="button-link" href="<?= base_url('Viajar/contato')?>"><button type="button" class="btn btn-default px-3">Fazer orçamento!</button>
             </a>

            

        </div>

    </div>
    

    
    <div class="card mb-4">

        
        <div class="view overlay">
            <img class="card-img-top" src="<?= base_url('assets/img/logo13.jpg')?>" alt="Card image cap">
            
        </div>

        
        <div class="card-body">

            
            <h4 class="card-title">Para Descobrir</h4>
            
            <p class="card-text">Pise onde poucos pisaram, descubra paisagens que poucos descobriram.
                Com essa escolha sua viagem será uma descoberta de si mesmo e do meio a sua volta, locais onde poucos passaram, 
                    paisagens que tiram o folego e nos faz refletir em como o mundo pe grande.

                    Começamos com um investimento minimo de R$ 4000,00
            </p>
            
            <a class="button-link" href="<?= base_url('Viajar/contato')?>"><button type="button" class="btn btn-default px-3">Fazer orçamento!</button>
             </a>

            
        </div>

    </div>
    

</div>
</div>

<div class="justify-content">
<div class="card-deck">

   
    <div class="card mb-4">

        
        <div class="view overlay">
            <img class="card-img-top" src="<?= base_url('assets/img/logo14.jpg')?>" alt="Card image cap">
            
        </div>

        
        <div class="card-body">

            
            <h4 class="card-title">Para Relaxar</h4>
            
            <p class="card-text">Relaxe em praias deslubrantes, um lugar perfeito para recarregar as energias.
                 Com essa escolha sua viagem será relaxante, um momento de lazer e descanso, lugares que pensam na melhor forma
                    de acolher aqueles que querem apenas descansar e repor as energias.

                    Começamos com um investimento minimo de R$ 6000,00</p>
            
            <a class="button-link" href="<?= base_url('Viajar/contato')?>"><button type="button" class="btn btn-default px-3">Fazer orçamento!</button>
             </a>
           
        </div>

    </div>
    
    <div class="card mb-4">

        
        <div class="view overlay">
            <img class="card-img-top" src="<?= base_url('assets/img/logo15.jpg')?>" alt="Card image cap">
            
        </div>

        
        <div class="card-body">

            
            <h4 class="card-title">Para Conhecer</h4>
            
            <p class="card-text">Conheça culturas e antigos costumes, ideal para você que gosta de conhecer o passado.
                Com essa escolha sua viagem será cheia de experiencias e conhecimentos sobre novas culturas e 
                    seus costumes. Conhecerá o passado e os povos que originaram a nossa civilização.

                    Começamos com um investimento minimo de R$ 5500,00
            </p>
            
            <a class="button-link" href="<?= base_url('Viajar/contato')?>"><button type="button" class="btn btn-default px-3">Fazer orçamento!</button>
             </a>
           
        </div>

    </div>
    

    
    <div class="card mb-4">

        
        <div class="view overlay">
            <img class="card-img-top" src="<?= base_url('assets/img/logo16.jpg')?>" alt="Card image cap">
            
        </div>

        
        <div class="card-body">

            
            <h4 class="card-title">Para Comemorar</h4>
            
            <p class="card-text">Venha comemorar a chegada de um novo ano, lugar ideal para se iniciar um novo ciclo.
                Com essa escolha sua viagem será uma festa, comemorando o novo ciclo que está por vir e as consquistas do ciclo 
                    que passou, muita alegria e celebração em um novo ano. Comemore o Ano Novo em um novo lugar.

                    Começamos com um investimento minimo de R$ 9000,00
            </p>
            
            <a class="button-link" href="<?= base_url('Viajar/contato')?>"><button type="button" class="btn btn-default px-3">Fazer orçamento!</button>
             </a>


        </div>

    </div>-->

