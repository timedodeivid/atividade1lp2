


<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="<?=base_url('assets/img/img17.jpg')?>">
      <div class="carousel-caption">
        <div class="animated fadeInDown">
          <h3 class="h3-responsive">
          Somos uma equipe preparada para disponibilizar aos nossos clientes novas experiências. Uma nova forma
          de viajar. Você precisa apenas escolher qual tipo de viagem gostaria, com aventura, para relaxar, para conhecer novas culturas, para cutir, o que desejar.Então,
          com base nessa escolha e no valores investidos  que serão disposnibilizados por nossos clientes, planejamos o local da viagem, onde ficarão e etc, sem dores de cabeça. Dessa forma 
          proporcionamos uma surpresa com o melhor custo beneficio disponivel com os valores disponibilizados. Queremos proporcionar a nossos clientes 
          uma grande e agradável surpresa de viajar com tranquilidade, sem preocupações e para o melhor destino possivel.
          Explore nossas alternativas, entre em contato e tenha uma ótima experiência assim como muitos já tiveram.
          Estamos ansiosos para realizar o seu sonho. 
          </h3>
          
        </div>
      </div>
    </div>
    </div>
    


<section class="team-section text-center my-5">

  
  <h2 class="h1-responsive font-weight-bold my-5">Depoimentos e Comentários</h2>
  
  <p class="dark-grey-text w-responsive mx-auto mb-5">Algumas opiniões e comentários de quem já teve a experiencia
  de viver uma viagemVIVA.
  </p>

  
  <div class="row text-center">

    
    <div class="col-md-4 mb-md-0 mb-5">

      <div class="testimonial">
        
        <div class="avatar mx-auto">
          <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" class="rounded-circle z-depth-1 img-fluid">
        </div>
        
        <h4 class="font-weight-bold dark-grey-text mt-4">Ana Oliveira</h4>
        <h6 class="font-weight-bold blue-text my-3">Publicitária</h6>
        <p class="font-weight-normal dark-grey-text">
          <i class="fas fa-quote-left pr-2"></i>Super recomendo, uma experiência maravilhosa</p>
        
        <div class="orange-text">
          <i class="fas fa-star"> </i>
          <i class="fas fa-star"> </i>
          <i class="fas fa-star"> </i>
          <i class="fas fa-star"> </i>
          <i class="fas fa-star-half-alt"> </i>
        </div>
      </div>

    </div>
    

    
    <div class="col-md-4 mb-md-0 mb-5">

      <div class="testimonial">
        
        <div class="avatar mx-auto">
          <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(9).jpg" class="rounded-circle z-depth-1 img-fluid">
        </div>
        
        <h4 class="font-weight-bold dark-grey-text mt-4">João Santos</h4>
        <h6 class="font-weight-bold blue-text my-3">Analista de negócios</h6>
        <p class="font-weight-normal dark-grey-text">
          <i class="fas fa-quote-left pr-2"></i>Eu precisava de um tempo para relaxar, foi maravilhoso, sem preocupações
          apenas recarregando as energias</p>
        
        <div class="orange-text">
          <i class="fas fa-star"> </i>
          <i class="fas fa-star"> </i>
          <i class="fas fa-star"> </i>
          <i class="fas fa-star"> </i>
          <i class="fas fa-star"> </i>
        </div>
      </div>

    </div>
    

    
    <div class="col-md-4">

      <div class="testimonial">
        
        <div class="avatar mx-auto">
          <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(12).jpg" class="rounded-circle z-depth-1 img-fluid">
        </div>
        
        <h4 class="font-weight-bold dark-grey-text mt-4">Sara Meleck</h4>
        <h6 class="font-weight-bold blue-text my-3">Paisagista</h6>
        <p class="font-weight-normal dark-grey-text">
          <i class="fas fa-quote-left pr-2"></i>Novas culturas, novas descobertas. Que viagem rica. Está mais que recomendado.</p>
        
        <div class="orange-text">
          <i class="fas fa-star"> </i>
          <i class="fas fa-star"> </i>
          <i class="fas fa-star"> </i>
          <i class="fas fa-star"> </i>
          <i class="far fa-star"> </i>
        </div>
      </div>

    </div>
   
  </div>
  

</section>
