
<div class='float-left btn-group'>
<a class="btn btn-default px-3" role="button" href="<?= base_url('Viajar/admincadastrar')?>">Cadastrar nova oferta</a>
<a class="btn btn-default px-3" role="button" href="<?= base_url('Viajar/adminlistar')?>">Listar</a>

</div>    
</div><br/><br/>

<div class="btn-default text-center  mt-5">
						<h3>Ofertas Cadastradas</h3>
</div>

					<table class="table table-hover">
						<thead>
							<tr>
								<th scope="col" class="text-center">ID</th>
								<th scope="col" class="text-center">Imagem</th>
								<th scope="col" class="text-center">Título</th>
								<th scope="col" class="text-center">Descrição</th>
								<th scope="col" class="text-center">Valor</th>
							</tr>
						</thead>
						<tbody>
                        <?php foreach ($ofertas as $oferta): ?>
					
						
						<tr>
                            <td class="text-center"><?= $oferta->id ?></td>
							<td class="text-center"><?= $oferta->img_card ?></td>
							<td class="text-center"><?= $oferta->titulo_card ?></td>
							<td class="text-center"><?= $oferta->msg_card ?></td>
							<td class="text-center">R$ <?= $oferta->preco_card ?>,00</td>
                            <td class="text-center"><a class="btn btn-default px-3" role="button" href="<?= base_url('Viajar/adminalterar/'.$oferta->id)?>">Alterar</a></td>
                            <td class="text-center"><a class="btn btn-default px-3" role="button" href="<?= base_url('Viajar/adminexcluir/'.$oferta->id)?>">Excluir</a></td>
                            
							
						</tr>
                        

                        <?php  endforeach;  ?>

					
						</tbody>
					</table>
				

